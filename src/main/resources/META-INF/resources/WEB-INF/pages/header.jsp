<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<!--header--->

<div class="header-top-w3layouts">
	<div class="container">
		<div class="col-md-6 logo-w3">
			<a href="queenberindia.com"><img
				src="<c:url value="/images/Teammars_Small.png" />" alt=" "
				border="0"></a>
		</div>
		<div class="col-md-6 logo-w3"></div>

		<div class="clearfix"></div>
	</div>
</div>
