<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<div class="container-fluid">
	<form action="<c:url value="login" />" method="post">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<form role="form">
					<div class="form-group">

						<label for="username"> User Name </label> <input type="email"
							class="form-control" id="username" />
					</div>
					<div class="form-group">

						<label for="password"> Password </label> <input type="password"
							class="form-control" id="password" />
					</div>

					<button type="submit" class="btn btn-primary" >Submit</button>
				</form>
			</div>
			<div class="col-md-4"></div>
		</div>
	</form>
</div>
