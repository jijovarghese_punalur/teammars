<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>TeamMars</title>
<!-- css -->
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css" media="all" />
<link rel="stylesheet" href="<c:url value="/css/style.css" />"
	type="text/css" media="all" />


<script src="<c:url value="js/jquery-1.11.1.min.js" />"></script>
<script src="<c:url value="js/bootstrap.js" />"></script>
</head>
<body>
	<jsp:include page="/WEB-INF/pages/header.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/pages/teammars_login.jsp"></jsp:include>
</body>
</html>