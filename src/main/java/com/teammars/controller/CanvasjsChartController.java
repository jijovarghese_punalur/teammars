/**
 * 
 */
package com.teammars.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.teammars.chart.service.CanvasjsChartService;

/**
 * @author hellboy
 *
 */
@Controller
public class CanvasjsChartController {
	@Autowired
	private CanvasjsChartService canvasjsChartService;
 
	@RequestMapping(value="/canvasjschart", method = RequestMethod.GET)
	public String springMVC(ModelMap modelMap) {
		return "chart/crosshair";
	}
	
	@RequestMapping(value = "/restfull-service/eBay-inc-stock-price.json", method = RequestMethod.GET)
	public @ResponseBody String getDataPoints(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		return canvasjsChartService.getCanvasjsChartData();
	}
 
}