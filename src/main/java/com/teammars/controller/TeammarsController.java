/**
 * 
 */
package com.teammars.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author hellboy
 *
 */
@Controller
public class TeammarsController {
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String getMethod(){
		return "index";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String login(){
		return "redirect:/canvasjschart";
	}

}
