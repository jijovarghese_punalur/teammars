/**
 * 
 */
package com.teammars.chart.service;



/**
 * @author hellboy
 *
 */
public interface CanvasjsChartService {
	String getCanvasjsChartData();
}
