/**
 * 
 */
package com.teammars.chart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teammars.chart.dao.CanvasjsChartDao;
import com.teammars.chart.service.CanvasjsChartService;

/**
 * @author hellboy
 *
 */
@Service
public class CanvasjsChartServiceImpl implements CanvasjsChartService {
	 
	@Autowired
	private CanvasjsChartDao canvasjsChartDao;
 
	public void setCanvasjsChartDao(CanvasjsChartDao canvasjsChartDao) {
		this.canvasjsChartDao = canvasjsChartDao;
	}
 
	@Override
	public String getCanvasjsChartData() {
		return canvasjsChartDao.getCanvasjsChartData();
	}
 
} 
