/**
 * 
 */
package com.teammars.chart.dao.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import com.teammars.chart.dao.CanvasjsChartDao;

/**
 * @author hellboy
 * 
 */
@Service
public class CanvasjsChartDaoImpl implements CanvasjsChartDao {

	@Autowired
	ResourceLoader resourceLoader;

	@Override
	public String getCanvasjsChartData() {
		return getCanvasjsDataList();
	}

	private String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	public JSONArray readJsonFromUrl(String url) throws IOException,
			JSONException {
		Resource resource = resourceLoader.getResource("classpath:" + url);
		InputStream is = resource.getInputStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONArray json = new JSONArray(jsonText);
			return json;
		} finally {
			is.close();
		}
	}

	public String getCanvasjsDataList() {
		JSONArray json = null;
		try {
			json = readJsonFromUrl("eBay-inc-stock-price.json");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json.toString();
	}
}