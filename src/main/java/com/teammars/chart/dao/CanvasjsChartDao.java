/**
 * 
 */
package com.teammars.chart.dao;

/**
 * @author hellboy
 *
 */
public interface CanvasjsChartDao {
	String getCanvasjsChartData();
}
