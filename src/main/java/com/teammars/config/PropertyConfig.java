/**
 * 
 */
package com.teammars.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author hellboy
 *
 */
@ConfigurationProperties(prefix="sts-demo")
public class PropertyConfig {
	private String demo;

	public String getDemo() {
		return demo;
	}

	public void setDemo(String demo) {
		this.demo = demo;
	}

}
