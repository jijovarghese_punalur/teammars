package com.teammars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class TeammarsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeammarsApplication.class, args);
	}

}
